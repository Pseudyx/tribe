﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace Tribe.Core.Web.Infrastructure
{
    
    using Core.Infrastructure;
    using Interface;
    using Manager;
    public class ApplicationWebUser : ApplicationUser, IApplicationUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager userManager, string authenticationType)
        {
            var userIdentity = await userManager.CreateIdentityAsync(this, authenticationType);
            //add custom user claims here
            return userIdentity;
        }
    }
}
