﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;


namespace Tribe.Core.Web
{
    using Core.Infrastructure;
    using Repository;
    using Service;
    using Infrastructure;
    public class ComponentInstaller : IWindsorInstaller
    {
        private string _connectionString = "Core";

        public ComponentInstaller(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<EntityDbContext>()
                    .DependsOn(Dependency.OnValue<string>(_connectionString))
                    .LifestyleTransient());

            container.Register(Component.For<ApplicationUserRole>()
              .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationUserLogin>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationUserClaim>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationRole>()
               .LifestylePerWebRequest());

            container.Register(Component.For<ApplicationWebUser>()
               .LifestylePerWebRequest());

            container.Register(Classes.FromThisAssembly()
                .BasedOn<EntityService>()
                .LifestyleTransient()
                .Configure(x => x.Named(x.Implementation.FullName)));
        }
    }
}
