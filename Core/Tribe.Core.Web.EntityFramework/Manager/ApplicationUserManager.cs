﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using Owin;

namespace Tribe.Core.Web.Manager
{
    using Infrastructure;
    public class ApplicationUserManager : UserManager<ApplicationWebUser, Guid>
    {
        public ApplicationUserManager(IUserStore<ApplicationWebUser, Guid> store) : base(store) { }

        public static ApplicationUserManager Create(IUserStore<ApplicationWebUser, Guid> store, IAppBuilder appBuilder, IIdentityMessageService emailService, IIdentityMessageService smsService)
        {
            var manager = Create(store, appBuilder, emailService);
            manager.SmsService = smsService;
            return manager;
        }

        public static ApplicationUserManager Create(IUserStore<ApplicationWebUser, Guid> store, IAppBuilder appBuilder, IIdentityMessageService emailService)
        {
            var manager = Create(store, appBuilder);
            manager.EmailService = emailService;
            return manager;
        }

        public static ApplicationUserManager Create(IUserStore<ApplicationWebUser, Guid> store, IAppBuilder appBuilder)
        {
            var manager = new ApplicationUserManager(store);

            manager.UserValidator = new UserValidator<ApplicationWebUser, Guid>(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true
            };

            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            var dataProtectionProvider = appBuilder.GetDataProtectionProvider();
            if (dataProtectionProvider != null)
            {
                var dataProtector = dataProtectionProvider.Create("ASP.NET Identity");
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationWebUser, Guid>(dataProtector);
            }

            return manager;
        }
    }
}
