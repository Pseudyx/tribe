﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Tribe.Core.Web.Manager;

namespace Tribe.Core.Web.Api
{
    public class WebApiController : ApiController
    {
        public System.Net.HttpStatusCode status;
        public string message = String.Empty;

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public WebApiController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        //public void AddErrors(IdentityResult result)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    if (!string.IsNullOrEmpty(message))
        //    {
        //        sb.Append(message);
        //    }

        //    foreach (var error in result.Errors)
        //    {
        //        sb.AppendLine(error);
        //    }
        //    message = sb.ToString();
        //}
    }
}
