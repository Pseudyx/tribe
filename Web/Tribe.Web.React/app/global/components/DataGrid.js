var React = require('react');
var DataTable = require('../../global/components/DataTable')

var DataGrid = React.createClass({
    handleRowClick: function(rowId){
        this.props.onRowClick(rowId);
    },
    render: function(){
        return(
            <div className="table-responsive">
                <div>Drag Column here:</div>

                <table className="table table-striped">
                    <thead>
                        <tr id="group" >
                            <td>1</td>
                        </tr>
                        <tr id="columns">
                            <td>2</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>3</td>
                        </tr>
                    </tbody>
                </table>

                <DataTable dataSource={this.props.dataSource} onRowClick={this.handleRowClick} />
            </div>
        )
    }
});

module.exports = DataGrid;