var React = require('react');

var DataTable = React.createClass({
    handleRowClick: function(e){
        this.props.onRowClick(e.currentTarget.getAttribute('itemID'));
    },
    dataBind: function(){
        if(this.props.dataSource != undefined){
        var colArr = [];
        for(var col in this.props.dataSource[0]) {
            if(col.toString() === 'ID') continue;
            colArr.push(col.toString())
        };
        var head = colArr.map(function(col, key){
            return <th key={key}>{col}</th>
        })
        var table = this.props.dataSource.map(function(rows, key) {
             var row = colArr.map(function(col, key){
                 return <td key={key}>{rows[col]}</td>
             })
            return(<tr itemID={rows.ID} key={key} onClick={this.handleRowClick}>{row}</tr>);
        }.bind(this))

        return{
            head: head,
            rows: table
        };
        }
    },
    render: function(){
        var dataTable = this.dataBind();
        return(
            <div className="table-responsive">
        <table className="table table-striped">
            <thead>
                <tr>
                    {dataTable.head}
                </tr>
            </thead>
            <tbody>
                {dataTable.rows}
            </tbody>
        </table>
    </div>
        )
    }
});

module.exports = DataTable;

