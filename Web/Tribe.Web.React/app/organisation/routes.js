var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var hashHistory = ReactRouter.hashHistory;
var IndexRoute = ReactRouter.IndexRoute;
var Main = require('./modules/Main');
var Overview = require('./modules/Overview');
var Organisation = require('./modules/Organisation');
var People = require('./modules/People');

var routes = (
    <Router history={hashHistory}>
        <Route path='/' component={Main}>
            <IndexRoute component={Overview} />
            <Route path='Organisation' header='Organisation' component={Organisation} />
            <Route path='People' header='People' component={People} />
        </Route>
    </Router>
)

module.exports = routes;