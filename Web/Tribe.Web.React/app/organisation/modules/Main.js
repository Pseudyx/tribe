var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link

var Main = React.createClass({
    render: function(){
        //console.log(this.props)
        var currentRoute = this.props.location.pathname;
        var routeNodes = this.props.route.childRoutes.map(function(route, key) {
            var activeClass = (currentRoute == '/'+route.path) ? "active" : "";
            return (
                <li className={activeClass} key={key}><Link to={route.path}>{route.header}</Link></li>
            );
        });
        return(
            <div className="row">
                <div className="col-sm-3 col-md-2 sidebar">
                    <ul className="nav nav-sidebar">
                        <li className={(currentRoute == '/') ? 'active' : ''}><Link to="/">Overview</Link></li>
                        {routeNodes}
                    </ul>
                </div>
                <div className="col-sm-9 offset-sm-3 col-md-10 offset-md-2 main">
                  {this.props.children}
                </div>
            </div> 
        )
    }
});

module.exports = Main;