var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link
var axios = require('axios');
var DataGrid = require('../../global/components/DataGrid')
var transparentBg = require('../../global/styles').transparentBg;

var PropTypes = React.PropTypes;

var Knight = React.createClass({
  render: function () {
    return <span>♘</span>;
  }
});

var Square = React.createClass({
  propTypes: {
    black: PropTypes.bool
  },

  render: function () {
    var black = this.props.black;
    var fill = black ? 'black' : 'white';
    var stroke = black ? 'white' : 'black';

    return (
      <div style={{
        backgroundColor: fill,
        color: stroke,
        width: '100%',
        height: '100%'
      }}>
        {this.props.children}
      </div>
    );
  }
});

var Board = React.createClass({
  propTypes: {
    knightPosition: PropTypes.arrayOf(
      PropTypes.number.isRequired
    ).isRequired
  },

  renderSquare: function (i) {
    var x = i % 8;
    var y = Math.floor(i / 8);
    var black = (x + y) % 2 === 1;

    var knightX = this.props.knightPosition[0];
    var knightY = this.props.knightPosition[1];
    var piece = (x === knightX && y === knightY) ?
      <Knight /> :
      null;

    return (
      <div key={i}
           style={{ width: '12.5%', height: '20' }}>
        <Square black={black}>
          {piece}
        </Square>
      </div>
    );
  },

  render: function () {
    var squares = [];
    for (i = 0; i < 64; i++) {
      squares.push(this.renderSquare(i));
    }

    return (
      <div style={{
        width: '100%',
        height: '100%',
        display: 'flex',
        flexWrap: 'wrap'
      }}>
        {squares}
      </div>
    );
  }
});

var Organisation = React.createClass({
    getInitialState: function(){
        return {
            data: []
        }
    },
    componentWillMount: function(){
        var _this = this;
        this.serverRequest = 
        axios
            .get("http://codepen.io/jobs.json")
            .then(function(result){
                    _this.setState({
                        data: result.data.jobs
                    }, function(){console.log('callback')});
                   /*this.forceUpdate();*/
                }.bind(this));
    },
    componentWillUnmount: function() {
        //this.serverRequest.abort();
    },
    handleRowClick: function(rowId){
        alert(rowId);
    },
    render: function(){
     
        return(
            <Board knightPosition={this.props.knightPosition} />
            /*<DataGrid dataSource={this.state.data} onRowClick={this.handleRowClick} />*/
        )
    }
});

module.exports = Organisation;