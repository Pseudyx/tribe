var HtmlWebpackPlugin = require('html-webpack-plugin')
/*var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/app/index.html',
  filename: 'index.html',
  inject: 'body'
});*/
var distRoot = '../Tribe.Web.UI/app';

var organisationConfig = {
    name: "organisation",
    entry: "./app/organisation/index.js",
    output: {
       path: distRoot,
       filename: "module.organisation.js"
    },
    module: {
      loaders: [
        {test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"}
      ]
    },
  //plugins: [HTMLWebpackPluginConfig]
}

// Return Array of Configurations
module.exports = [
    organisationConfig    	
];