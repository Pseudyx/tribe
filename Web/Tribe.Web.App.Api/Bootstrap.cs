﻿using System;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Framework.Web.Api.Factory;
using Tribe.Core.Web.Factory;

namespace Tribe.Web.App.Api
{
    public class Bootstrap
    {

        private static readonly Lazy<IWindsorContainer> container = new Lazy<IWindsorContainer>(() =>
        {
            var app = new App.ComponentInstaller();
            var DAL = new DAL.ComponentInstaller();
            var core = new Core.Web.ComponentInstaller("App");
            
            var container = new WindsorContainer()
                 .Install(FromAssembly.This())
                 .Install(app)
                 .Install(DAL)
                 .Install(core);
                 

            return container;
        });

        private Bootstrap()
        {
        }

        public static IWindsorContainer Container
        {
            get
            {
                return container.Value;
            }
        }

        public static void Register()
        {
            GlobalConfiguration.Configuration.Services.Replace(
                typeof(IHttpControllerActivator),
                new ApiConrtollerFactory(Container));

            var controllerFactory = new ControllerFactory(Container.Kernel);
            App.Bootstrap.ControllerInjector(controllerFactory);
        }
    }
}
