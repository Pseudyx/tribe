﻿using System;
using System.Data.Entity;
using System.Web;
using System.Web.Http;
using Castle.MicroKernel.Registration;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;

using Tribe.Core.Infrastructure;
using Tribe.Core.Repository;
using Tribe.Core.Web.Infrastructure;
using Tribe.Core.Web.Manager;
using Tribe.Core.Web.Provider;
using Tribe.Web.App.Api.Config;
using Tribe.Web.App.Factories;

//[assembly: OwinStartup(typeof(Tribe.Web.App.Api.Startup))]
namespace Tribe.Web.App.Api
{
    public class ApiApplication : Application
    {
        public override void Register()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            base.Register();
        }

        public override void Application_Start()
        {
            Register();
            Bootstrap.Register();
        }
    }

    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        public static string PublicClientId { get; private set; }

        static Startup()
        {
            PublicClientId = "AppClient";

            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                AuthorizeEndpointPath = new PathString("/Account/Authorize"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(7),
                AllowInsecureHttp = true
            };
        }

        public virtual void Configuration(IAppBuilder app)
        {
            var container = Bootstrap.Container;

            container.Register(
                Component
                    .For<IAppBuilder>()
                    .Instance(app),
                Component
                    .For<IUserStore<ApplicationWebUser, Guid>>()
                    .ImplementedBy<UserStore<ApplicationWebUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>>()
                    .DependsOn(Dependency.OnComponent<DbContext, EntityDbContext>())
                    .LifestyleTransient(),
                Component
                    .For<ApplicationUserManager>()
                    .UsingFactoryMethod(kernel =>
                        ApplicationUserManager.Create(
                            kernel.Resolve<IUserStore<ApplicationWebUser, Guid>>(),
                            kernel.Resolve<IAppBuilder>(),
                            new EmailerFactory()))
                    .LifestyleTransient(),
                Component
                    .For<IAuthenticationManager>()
                    .UsingFactoryMethod(kernel => HttpContext.Current.GetOwinContext().Authentication)
                    .LifestyleTransient(),
                Component
                    .For<ApplicationSignInManager>()
                    .LifestyleTransient());

            app.CreatePerOwinContext(() => container.Resolve<ApplicationUserManager>());

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationWebUser, Guid>(
                        validateInterval: TimeSpan.FromMinutes(15),
                        regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager, DefaultAuthenticationTypes.ApplicationCookie),
                        getUserIdCallback: (id) => new Guid(id.GetUserId()))
                }
            });

            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
        }
    }
}
