﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Tribe.Core.Web.Api;
using Tribe.Core.Web.Manager;
using Tribe.Web.App.Models;
using Newtonsoft.Json;
using System.Web.Http.Results;

namespace Tribe.Web.App.Api.Controllers
{
    public class OrganisationController : WebApiController
    {
        private IList<CommentModel> _comments;

        public OrganisationController(ApplicationUserManager userManager) : base(userManager) 
        {
            _comments = new List<CommentModel>
            {
                new CommentModel
                {
                    Id = 1,
                    Author = "Daniel Lo Nigro",
                    Text = "Hello ReactJS.NET World!"
                },
                new CommentModel
                {
                    Id = 2,
                    Author = "Pete Hunt",
                    Text = "This is one comment"
                },
                new CommentModel
                {
                    Id = 3,
                    Author = "Jordan Walke",
                    Text = "This is *another* comment"
                },
            };
        }

        [HttpGet]
        public IHttpActionResult Comments()
        {
            return new JsonResult<IList<CommentModel>>(_comments, new JsonSerializerSettings(), Encoding.UTF8, this);
            //return Json(_comments, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public IHttpActionResult AddComment(CommentModel comment)
        {
            // Create a fake ID for this comment
            comment.Id = _comments.Count + 1;
            _comments.Add(comment);
            return Content<string>(System.Net.HttpStatusCode.OK, "Success :)");
        }
    }
}
