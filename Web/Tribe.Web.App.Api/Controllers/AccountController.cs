﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Tribe.Core.Web.Api;
using Tribe.Core.Web.Api.Response;
using Tribe.Core.Web.Infrastructure;
using Tribe.Core.Web.Manager;
using Tribe.DAL.Domain;
using Tribe.DAL.Services;
using Tribe.Web.App.Models;

namespace Tribe.Web.App.Api.Controllers
{
    public class AccountController : WebApiController
    {
        private AccountService _accountService;
        public AccountService AccountService
        {
            get
            {
                return _accountService;
            }
            private set
            {
                _accountService = value;
            }
        }

        public AccountController(ApplicationUserManager userManager, AccountService accountService) : base(userManager) 
        {
            AccountService = accountService;
        }

       
        [HttpPost]
        public async Task<IHttpActionResult> AddUser(CreateUserViewModel model)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var user = new ApplicationWebUser
                    {
                        UserName = model.Email,
                        Email = model.Email,
                        JoinDate = DateTime.Now.Date,
                    };

                    var usr = await UserManager.FindByEmailAsync(model.Email);
                    var result = (usr == null) ? await UserManager.CreateAsync(user, model.Password) : IdentityResult.Failed(new string[] { $"A user with email {model.Email} already exists" });
                    if (result.Succeeded)
                    {
                        var profile = new Profile
                        {
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,
                            ApplicationWebUserId = user.Id
                        };

                        var profileId = await AccountService.AddProfile(profile);
                        status = System.Net.HttpStatusCode.OK;
                    }
                    else
                    {
                        status = System.Net.HttpStatusCode.BadRequest;
                        // AddErrors(result);
                    }

                }
                catch (Exception ex)
                {
                    status = System.Net.HttpStatusCode.InternalServerError;
                    message = ex.Message;
                }

                if (status == System.Net.HttpStatusCode.InternalServerError || status == System.Net.HttpStatusCode.BadRequest)
                {
                    return new ApiActionResult<String>(status, message);
                }
                else
                {
                    return new ApiActionResult(status);
                }
            }
            else
            {
                return new ApiActionResult<ModelStateDictionary>(System.Net.HttpStatusCode.BadRequest, ModelState);
            }
        }

        [Route("User")]
        [HttpPatch]
        public async Task<IHttpActionResult> PatchUser(PatchUserViewModel model)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var profile = AccountService.GetById<Profile, Guid>(model.ProfileId);

                    if (profile != null)
                    {
                        var user = new ApplicationWebUser
                        {
                            UserName = model.Email,
                            Email = model.Email,
                            JoinDate = DateTime.Now.Date,
                        };

                        var usr = await UserManager.FindByEmailAsync(model.Email);
                        var result = (usr == null) ? await UserManager.CreateAsync(user, model.Password) : IdentityResult.Failed(new string[] { $"A user with email {model.Email} already exists" });
                        if (result.Succeeded)
                        {
                            profile.ApplicationWebUserId = user.Id;
                            await AccountService.UpdateProfile(profile);
                            status = System.Net.HttpStatusCode.OK;
                        }
                        else
                        {
                            status = System.Net.HttpStatusCode.BadRequest;
                            // AddErrors(result);
                        }
                    }
                    else
                    {
                        status = System.Net.HttpStatusCode.BadRequest;
                        message = "Profile not found";
                    }
                }
                catch (Exception ex)
                {
                    status = System.Net.HttpStatusCode.InternalServerError;
                    message = ex.Message;
                }

                if (status == System.Net.HttpStatusCode.InternalServerError || status == System.Net.HttpStatusCode.BadRequest)
                {
                    return new ApiActionResult<String>(status, message);
                }
                else
                {
                    return new ApiActionResult(status);
                }
            }
            else
            {
                return new ApiActionResult<ModelStateDictionary>(System.Net.HttpStatusCode.BadRequest, ModelState);
            }
        }

        [Route("Person")]
        [HttpPost]
        public async Task<IHttpActionResult> AddPerson(ProfileViewModel model)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    var profile = new Profile
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Email = model.Email,
                        Phone = model.Phone,
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };

                    var profileId = await AccountService.AddProfile(profile);
                    status = System.Net.HttpStatusCode.OK;

                }
                catch (Exception ex)
                {
                    status = System.Net.HttpStatusCode.InternalServerError;
                    message = ex.Message;
                }

                if (status == System.Net.HttpStatusCode.InternalServerError || status == System.Net.HttpStatusCode.BadRequest)
                {
                    return new ApiActionResult<String>(status, message);
                }
                else
                {
                    return new ApiActionResult(status);
                }
            }
            else
            {
                return new ApiActionResult<ModelStateDictionary>(System.Net.HttpStatusCode.BadRequest, ModelState);
            }
        }
    }
}
