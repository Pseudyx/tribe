﻿using System.Data.Entity;



namespace Tribal.Web.App.Repository
{
    using Tribal.Core.Repository;
    using Tribal.Web.Domain;
    public class EntityRepository : EntityDbContext
    {
        public DbSet<Profile> Profiles { get; set; }
    }
}
