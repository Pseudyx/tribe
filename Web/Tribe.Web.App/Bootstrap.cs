﻿using System;
using System.Web.Mvc;
using Castle.Windsor;
using Castle.Windsor.Installer;
using Tribe.Core.Web.Factory;

namespace Tribe.Web.App
{
    public class Bootstrap
    {

        private static readonly Lazy<IWindsorContainer> container = new Lazy<IWindsorContainer>(() =>
        {
            var core = new Core.Web.ComponentInstaller("App");
            var DAL = new DAL.ComponentInstaller();

            var container = new WindsorContainer()
                 .Install(FromAssembly.This())
                 .Install(DAL)
                 .Install(core);
                 

            return container;
        });

        private Bootstrap()
        {
        }

        public static IWindsorContainer Container
        {
            get
            {
                return container.Value;
            }
        }

        public static void Register()
        {
            var controllerFactory = new ControllerFactory(Container.Kernel);
            ControllerInjector(controllerFactory);
        }

        public static void ControllerInjector(ControllerFactory controllerFactory)
        {
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }
    }
}
