﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Tribe.Web.App
{
    public class ComponentInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                .BasedOn<Controller>()
                .LifestyleTransient()
                .Configure(x => x.Named(x.Implementation.FullName)));

            container.Register(Classes.FromThisAssembly()
                .InNamespace("Tribe.Web.App.Models")
                .LifestyleTransient()
                .Configure(x => x.Named(x.Implementation.FullName))
                );
        }
    }
}
