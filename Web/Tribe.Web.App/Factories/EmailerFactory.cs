﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Tribe.Core.Extension;
using Tribe.Core.Helper;
using Tribe.Core.Service;

namespace Tribe.Web.App.Factories
{
    public class EmailerFactory : Emailer, IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {

            TemplateProperties prop;
            var email = message.ToEmail(out prop);
            string body = (prop != null) ? Template(prop.Name, prop.Hash) : message.Body;
            await SendEmailAsync(email, body);

        }

        public static string Template(string template, IDictionary<string, object> data)
        {
            try
            {
                var renderer = new EmbeddedRenderer(Assembly.GetExecutingAssembly(), "Tribal.Web.App.Templates");
                var tpl = renderer.Render($"{template}.html", data);

                return tpl;
            }
            catch
            {
                return null;
            }
        }

    }
}
