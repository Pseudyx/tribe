﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Tribe.Web.App.Models;

namespace Tribe.Web.App.Controllers
{
    public class TribeController : Controller
    {

        public async Task<ActionResult> Index()
        {
            if (Request.IsAuthenticated)
            {
                //CHECK ROLES AND DEFAULT START PAGE
                return View();
            } else
            {
                return RedirectToAction("Login", "Account");
            }

        }
    }
}
