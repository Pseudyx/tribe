﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Tribe.Core.Infrastructure
{
    public class ApplicationUserClaim : IdentityUserClaim<Guid> { }
}
