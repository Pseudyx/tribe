﻿using System;
using System.Threading;

namespace Tribe.Core.Infrastructure
{
    using Interface;
    public static class ApplicationUserFacade
    {
        public static Guid CurrentUserId
        {
            get
            {
                if (!(Thread.CurrentPrincipal.Identity is IApplicationUser))
                {
                    return default(Guid);
                }

                var identity = Thread.CurrentPrincipal.Identity as IApplicationUser;
                return identity.Id;
            }
        }

        public static string CurrentUserName
        {
            get
            {
                if (!(Thread.CurrentPrincipal.Identity is IApplicationUser))
                {
                    return "anon";
                }

                var identity = Thread.CurrentPrincipal.Identity as IApplicationUser;
                return identity.UserName;
            }
        }

        public static string CurrentUserEmail
        {
            get
            {
                if (!(Thread.CurrentPrincipal.Identity is IApplicationUser))
                {
                    return "anon";
                }

                var identity = Thread.CurrentPrincipal.Identity as IApplicationUser;
                return identity.Email;
            }
        }
    }
}
