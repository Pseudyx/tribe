﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Tribe.Core.Infrastructure
{
    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public ApplicationRole()
        {
            Id = Guid.NewGuid();
        }
    }
}
