﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace Tribe.Core.Service
{
    using Repository;
    public class EntityService 
    {
        public readonly EntityDbContext _dbContext;

        public EntityService()
        {
          
        }

        public EntityService(EntityDbContext entityDbContext)
        {
            _dbContext = entityDbContext;
        }

        public virtual IQueryable<TEntity> GetAll<TEntity>()
        {
            var dbSet = (_dbContext as EntityDbContext).Set(typeof(TEntity));
            return (IQueryable<TEntity>)dbSet;
        }

        public virtual TEntity GetById<TEntity, T>(T id)
        {
            var dbSet = (_dbContext as EntityDbContext).Set(typeof(TEntity));
            return (TEntity)dbSet.Find(id);
        }

        public virtual TEntity GetByApplicationUserId<TEntity, TKey>(TKey userId)
        {
            var dbSet = (_dbContext as EntityDbContext).Set(typeof(TEntity));
            var qry = dbSet.Where("ApplicationUserId == @0", userId);
            return ((IQueryable<TEntity>)qry).FirstOrDefault();
        }

        public virtual async Task<TEntity> GetAsyncByApplicationUserId<TEntity, TKey>(TKey userId)
        {
            var dbSet = (_dbContext as EntityDbContext).Set(typeof(TEntity));
            var qry = dbSet.Where("ApplicationUserId == @0", userId);
            return await ((IQueryable<TEntity>)qry).FirstOrDefaultAsync();
        }

        public virtual IEnumerable<TEntity> GetEnumerableByApplicationUserId<TEntity, TKey>(TKey userId)
        {
            var dbSet = (_dbContext as EntityDbContext).Set(typeof(TEntity));
            var qry = dbSet.Where("ApplicationUserId == @0", userId);
            return ((IQueryable<TEntity>)qry).AsEnumerable();
        }
    }

    public class BaseService
    {

    }
}
