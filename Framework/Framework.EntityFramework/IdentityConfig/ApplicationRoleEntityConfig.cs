﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Infrastructure;

namespace Tribe.Core.IdentityConfig
{
    public class ApplicationRoleEntityConfig : EntityTypeConfiguration<ApplicationRole>
    {
        public ApplicationRoleEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("Role", "Core");
                c.Properties(p => new
                {
                    p.Id,
                    p.Name
                });
            }).HasKey(p => p.Id);
            
            this.HasMany(c => c.Users).WithRequired().HasForeignKey(c => c.RoleId);
        }
    }
}
