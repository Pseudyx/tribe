﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Infrastructure;

namespace Tribe.Core.IdentityConfig
{
    public class ApplicationUserRoleEntityConfig : EntityTypeConfiguration<ApplicationUserRole>
    {
        public ApplicationUserRoleEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("UserRole", "Core");
                c.Properties(p => new
                {
                    p.UserId,
                    p.RoleId
                });
            })
            .HasKey(c => new { c.UserId, c.RoleId });
        }
    }
}
