﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Infrastructure;
using Tribe.Core.Web.Infrastructure;

namespace Tribe.Core.IdentityConfig
{
    public class ApplicationUserClaimEntityConfig : EntityTypeConfiguration<ApplicationUserClaim>
    {
        public ApplicationUserClaimEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("UserClaim", "Core");
                c.Properties(p => new
                {
                    p.Id,
                    p.UserId,
                    p.ClaimValue,
                    p.ClaimType
                });
            }).HasKey(c => c.Id);

        }
    }
}
