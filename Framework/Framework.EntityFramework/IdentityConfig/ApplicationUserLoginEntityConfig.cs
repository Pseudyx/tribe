﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Infrastructure;

namespace Tribe.Core.IdentityConfig
{
    public class ApplicationUserLoginEntityConfig : EntityTypeConfiguration<ApplicationUserLogin>
    {
        public ApplicationUserLoginEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("UserLogin", "Core");
                c.Properties(p => new
                {
                    p.UserId,
                    p.LoginProvider,
                    p.ProviderKey
                });
            }).HasKey(p => new { p.LoginProvider, p.ProviderKey, p.UserId });

        }
    }
}
