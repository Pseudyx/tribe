﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Infrastructure;
using Tribe.Core.Web.Infrastructure;

namespace Tribe.Core.IdentityConfig
{
    public class ApplicationUserEntityConfig : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserEntityConfig()
        {
            this.Map(c =>
            {
                c.ToTable("User", "Core");
                c.Properties(p => new
                {
                    p.Id,
                    p.AccessFailedCount,
                    p.Email,
                    p.EmailConfirmed,
                    p.PasswordHash,
                    p.PhoneNumber,
                    p.PhoneNumberConfirmed,
                    p.TwoFactorEnabled,
                    p.SecurityStamp,
                    p.LockoutEnabled,
                    p.LockoutEndDateUtc,
                    p.UserName,
                    p.JoinDate
                });
            }).HasKey(c => c.Id);
            this.HasMany(c => c.Logins).WithOptional().HasForeignKey(c => c.UserId);
            this.HasMany(c => c.Claims).WithOptional().HasForeignKey(c => c.UserId);
            this.HasMany(c => c.Roles).WithRequired().HasForeignKey(c => c.UserId);

        }
    }
}
