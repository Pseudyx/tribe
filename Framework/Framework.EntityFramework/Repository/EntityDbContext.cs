﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Tribe.Core.Repository
{
    using IdentityConfig;
    using Infrastructure;
    public class EntityDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public EntityDbContext() : base("Core")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public EntityDbContext(string connectionString) : base(connectionString)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static EntityDbContext Create()
        {
            return new EntityDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            
            modelBuilder.Configurations.Add(new ApplicationRoleEntityConfig());
            modelBuilder.Configurations.Add(new ApplicationUserEntityConfig());
            modelBuilder.Configurations.Add(new ApplicationUserLoginEntityConfig());
            modelBuilder.Configurations.Add(new ApplicationUserRoleEntityConfig());
            modelBuilder.Configurations.Add(new ApplicationUserClaimEntityConfig());
        }

    }
}
