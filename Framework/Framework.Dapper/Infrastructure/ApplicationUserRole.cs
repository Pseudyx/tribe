﻿using System;

namespace Tribe.Core.Infrastructure
{
    using Tribal.AspNet.Identity.Dapper;
    public class ApplicationUserRole : IdentityUserRole<Guid> { }
}
