﻿using System;

namespace Tribe.Core.Infrastructure
{
    using Tribal.AspNet.Identity.Dapper;
    public class ApplicationUserClaim : IdentityUserClaim<Guid> { }
}
