﻿using System;

namespace Tribe.Core.Infrastructure
{
    using Tribal.AspNet.Identity.Dapper;
    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public ApplicationRole()
        {
            Id = Guid.NewGuid();
        }
    }
}