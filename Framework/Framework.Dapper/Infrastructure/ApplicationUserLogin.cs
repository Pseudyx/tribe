﻿using System;

namespace Tribe.Core.Infrastructure
{
    using Tribal.AspNet.Identity.Dapper;
    public class ApplicationUserLogin : IdentityUserLogin<Guid> { }
}
