﻿using System;

namespace Tribe.Core.Repository
{
    using Infrastructure;
    using Tribal.AspNet.Identity.Dapper;
    public class EntityDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>
    {
        public EntityDbContext() : base("Core")
        {  
        }

        public EntityDbContext(string connectionString) : base(connectionString)
        {
        }

        public static EntityDbContext Create()
        {
            return new EntityDbContext();
        }

        public static EntityDbContext Create(string connectionString)
        {
            return new EntityDbContext(connectionString);
        }

    }
}
