﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Tribe.Core.Web.Api.Response
{
    public class ApiActionResult<T> : IHttpActionResult
    {
        System.Net.HttpStatusCode statusCode;
        T data;

        public ApiActionResult(System.Net.HttpStatusCode statusCode, T data)
        {
            this.statusCode = statusCode;
            this.data = data;
        }

        public HttpResponseMessage CreateResponse(System.Net.HttpStatusCode statusCode, T data)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Properties.Add(System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            HttpResponseMessage response = request.CreateResponse(statusCode, data);
            return response;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(CreateResponse(this.statusCode, this.data));

        }
    }

    public class ApiActionResult : IHttpActionResult
    {
        System.Net.HttpStatusCode statusCode;

        public ApiActionResult(System.Net.HttpStatusCode statusCode)
        {
            this.statusCode = statusCode;
        }

        public HttpResponseMessage CreateResponse(System.Net.HttpStatusCode statusCode)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            request.Properties.Add(System.Web.Http.Hosting.HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            HttpResponseMessage response = request.CreateResponse(statusCode);
            return response;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(CreateResponse(this.statusCode));

        }
    }

}
