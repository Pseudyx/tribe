﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tribe.Core.Entity
{
    public class ListEntity<TPrimaryKey>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public TPrimaryKey Id { get; set; }

        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }

    }
}
