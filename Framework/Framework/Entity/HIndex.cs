﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Tribe.Core.Entity
{
    /// <summary>
    /// Hierarchy Index
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public class HIndex<TKey>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public TKey Id { get; set; }

        public TKey ParentId { get; set; }
        public TKey ChildId { get; set; }
    }
}
