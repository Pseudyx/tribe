﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tribe.Core.Entity
{
    using Omu.ValueInjecter;
    public class Entity<TPrimaryKey>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public TPrimaryKey Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public void Inject(object obj)
        {
            this.InjectFrom(obj);
        }
    }
}
