﻿using System;
using Microsoft.AspNet.Identity;

namespace Tribe.Core.Interface
{
    public interface IApplicationUser: IUser<Guid>
    {
        DateTime JoinDate { get; set; }
        string Email { get; set; }
    }
}
