﻿using System.IO;

namespace Tribe.Core.Interface
{
    public interface IEmail
    {
        void AddHTML(string htmlText);
        void AddText(string text);
        void AddAttachment(Stream stream, string fileName, string mediaType);
        void Send();
    }
}
