﻿using System;
using System.Configuration;

namespace Tribe.Core.Listener
{
    using System.Diagnostics;
    using System.IO;
    public class TextTraceListener : TextWriterTraceListener, IDisposable
    {
        public static StreamWriter stream { get; private set; }

        private Action<string> ShowMessageCallback { get; set; }

        public TextTraceListener()
            : base()
        {
            var path = ConfigurationManager.AppSettings["LOG_FILE_LOCATION"];
            if (!File.Exists(path))
            {
                stream = File.CreateText(path);

            }
            else
            {
#if DEBUG
                stream = File.AppendText(path + "_DEBUG");
#else
				stream = File.AppendText(path);
#endif
            }
        }

        //Destructor
        ~TextTraceListener()
        {
            DisposeProperly(false);
        }

        public override void Write(string message)
        {
            WriteToLog(message);
        }

        public override void WriteLine(string message)
        {
            WriteToLog(message);
        }

        private static void WriteToLog(string message)
        {
            stream.Write(DateTime.Now.ToString("yyyy-MMM-dd:hh:mm:ss:mm - ") + message + Environment.NewLine);
        }

        void IDisposable.Dispose()
        {
            DisposeProperly(true);
            GC.SuppressFinalize(this);
        }

        private void DisposeProperly(bool disposing)
        {
            if (disposing)
            {
                if (stream != null)
                {
                    stream.Close();
                }
            }
        }
    }
}
