﻿using System;

namespace Tribe.Core.Listener
{
    using System.Diagnostics;
    public class TextActionTraceListener : TextWriterTraceListener
    {
        private Action<string> ShowMessageCallback { get; set; }

        public TextActionTraceListener(Action<string> showMessageCallback): base()
        {
            ShowMessageCallback = showMessageCallback;
        }

        public override void Write(string message)
        {
            ShowMessageCallback(message);
        }

        public override void WriteLine(string message)
        {
            ShowMessageCallback(message);
        }
    }
}
