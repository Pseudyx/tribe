﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Tribe.Core.Helper
{
    using DotLiquid;
    public class EmbeddedRenderer
    {

        private readonly Assembly _assembly;
        private readonly string _folderNamespace;

        public EmbeddedRenderer(Assembly assembly, string folderNamespace)
        {
            _assembly = assembly;
            _folderNamespace = folderNamespace;
        }

        public string Render(string templateFilename, dynamic data = null)
        {
            var tpl = Template.Parse(ReadFile($"{_folderNamespace}.{templateFilename}", _assembly));
            return (data == null) ? tpl.Render() : tpl.Render(Hash.FromAnonymousObject(data));
        }

        public string Render(string templateFilename, IDictionary<string, object> data = null)
        {
            var tpl = Template.Parse(ReadFile($"{_folderNamespace}.{templateFilename}", _assembly));
            return (data == null) ? tpl.Render() : tpl.Render(Hash.FromDictionary(data));
        }

        private string ReadFile(string fileLocation, Assembly assembly)
        {
            if (!string.IsNullOrEmpty(fileLocation))
            {
                using (Stream stream = assembly.GetManifestResourceStream(fileLocation))
                {
                    if (stream != null)
                        using (var reader = new StreamReader(stream))
                            return reader.ReadToEnd();
                }
            }
            throw new FileNotFoundException();
        }
    }
}
