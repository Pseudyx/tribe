﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace Tribe.Core.Helper
{
    public static class ImageCompression
    {
        public static byte[] Compress(byte[] bytes, ImageFormat format)
        {
            byte[] result = null;

            ImageCodecInfo codecInfo = GetEncoderInfo(format);
            EncoderParameters parameters = new EncoderParameters(1);
            parameters.Param[0] = new EncoderParameter(Encoder.Quality, 45L);

            using (var ms = new MemoryStream(bytes))
            using (var image = Image.FromStream(ms))
            using (var streamout = new MemoryStream())
            {
                image.Save(streamout, codecInfo, parameters);
                streamout.Position = 0;
                result = new byte[streamout.Length];
                streamout.Write(result, 0, (int)streamout.Length);
            }

            return result;
        }

        public static ImageCodecInfo GetEncoderInfo(ImageFormat format)
        {
            return ImageCodecInfo.GetImageEncoders().FirstOrDefault(x => x.FormatID == format.Guid);   
        }
    }
}
