﻿using System.Threading.Tasks;

namespace Tribe.Core.Service
{
    public class Emailer
    {
        public static void SendEmail(Email email, string htmlBody)
        {
            email.AddHTML(htmlBody);
            SendEmail(email);
        }

        public static void SendEmail(Email email)
        {
            email.Send();
        }

        public static async Task SendEmailAsync(Email email, string htmlBody)
        {
            email.AddHTML(htmlBody);
            await SendEmailAsync(email);
        }

        public static async Task SendEmailAsync(Email email)
        {
            await email.SendAsync();
        }
    }
}
