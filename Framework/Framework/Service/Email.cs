﻿using System;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Tribe.Core.Service
{
    using Interface;
    public class Email : IEmail
    {
        MailMessage _mailMessage;

        public Email(string to, string subject)
        {
            var toAddress = new MailAddress(to);

            Initiate(new[] { toAddress }, subject);
        }

        public Email(string from, string to, string subject)
        {
            var fromAddress = new MailAddress(from);
            var toAddress = new MailAddress(to);

            Initiate(fromAddress, new[] { toAddress }, subject);
        }

        public Email(MailAddress[] toAddresses, string subject)
        {
            Initiate(toAddresses, subject);
        }

        public Email(MailAddress fromAddress, MailAddress[] toAddresses, string subject)
        {
            Initiate(fromAddress, toAddresses, subject);
        }

        public Email(MailAddress fromAddress, MailAddress[] toAddresses, MailAddress[] bccAddresses, string subject)
        {
            Initiate(fromAddress, toAddresses, bccAddresses, subject);
        }

        private void Initiate(MailAddress[] toAddresses, string subject)
        {
            _mailMessage = new MailMessage();

            foreach (var address in toAddresses)
            {
                _mailMessage.To.Add(address);
            }

            _mailMessage.Subject = subject;
        }

        private void Initiate(MailAddress fromAddress, MailAddress[] toAddresses, string subject)
        {
            _mailMessage = new MailMessage();
            _mailMessage.From = fromAddress;

            foreach (var address in toAddresses)
            {
                _mailMessage.To.Add(address);
            }

            _mailMessage.Subject = subject;
        }

        private void Initiate(MailAddress fromAddress, MailAddress[] toAddresses, MailAddress[] bccAddresses, string subject)
        {
            Initiate(fromAddress, toAddresses, subject);

            foreach (MailAddress bcc in bccAddresses)
            {
                _mailMessage.Bcc.Add(bcc);
            }
        }

        public void AddHTML(string htmlText)
        {
            Byte[] bytes = Encoding.UTF8.GetBytes(htmlText);
            var htmlStream = new MemoryStream(bytes);
            var htmlView = new AlternateView(htmlStream, MediaTypeNames.Text.Html);

            _mailMessage.AlternateViews.Add(htmlView);
        }

        public void AddText(string text)
        {
            Byte[] bytes = Encoding.UTF8.GetBytes(text);
            var textStream = new MemoryStream(bytes);
            var textView = new AlternateView(textStream, MediaTypeNames.Text.Plain);

            _mailMessage.AlternateViews.Add(textView);
        }

        public void AddAttachment(Stream stream, string fileName, string mediaType)
        {
            _mailMessage.Attachments.Add(new Attachment(stream, fileName, mediaType));
        }

        public void AddBcc(string bccAddress)
        {
            var bcc = new MailAddress(bccAddress);
            _mailMessage.Bcc.Add(bcc);
        }

        public void AddBcc(MailAddress[] bccAddresses)
        {
            foreach (MailAddress bcc in bccAddresses)
            {
                _mailMessage.Bcc.Add(bcc);
            }
        }

        public void Send()
        {
            try
            {
                new SmtpClient().Send(_mailMessage);
            }
            catch (Exception ex)
            {
                //Implement Log
            }

            //if default smtp provider settings in web.config is not enough, then implement customer settings here
        }

        public async Task SendAsync()
        {
            try
            {
                await new SmtpClient().SendMailAsync(_mailMessage);
            }
            catch (Exception ex)
            {
                //Implement Log
            }

            //if default smtp provider settings in web.config is not enough, then implement customer settings here
        }
    }
}
