﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;

namespace Tribe.Core.Extension
{ 
    using Service;
    public static class EmailExtension
    {
        public static Email ToEmail(this IdentityMessage message, out TemplateProperties properies)
        {
            if (message.Subject.Contains("Template;"))
            {
                string opts = message.Subject.Split(';')[1];
                string subject = message.Subject.Split(';')[2];
                string tpl = opts.Split('|')[0];
                string rawJson = opts.Split('|')[1];
                JObject data = (!string.IsNullOrEmpty(rawJson)) ? JObject.Parse(rawJson) : null;
                var dict = new Dictionary<string, object>();
                foreach (JProperty prop in data.Properties())
                {
                    dict.Add(prop.Name, prop.Value.ToString());
                }
                dict.Add("message", message.Body);

                properies = new TemplateProperties
                {
                    Name = tpl,
                    Subject = subject,
                    Hash = dict
                };

                return new Email(message.Destination, subject);
            }

            properies = null;
            return new Email(message.Destination, message.Subject);
        }
    }

    public class TemplateProperties
    {
        public string Name { get; set; }
        public string Subject { get; set; }
        public Dictionary<string, object> Hash { get; set; }

    }
}
