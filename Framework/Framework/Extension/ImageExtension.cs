﻿using System.Drawing.Imaging;

namespace Tribe.Core.Extension
{
    using Helper;
    public static class ImageExtensions
    {
        public static byte[] CompressJpeg(this byte[] bytes)
        {
            return ImageCompression.Compress(bytes, ImageFormat.Jpeg);
        }

        public static byte[] CompressGif(this byte[] bytes)
        {
            return ImageCompression.Compress(bytes, ImageFormat.Gif);
        }

        public static byte[] CompressPng(this byte[] bytes)
        {
            return ImageCompression.Compress(bytes, ImageFormat.Png);
        }

        public static byte[] CompressTiff(this byte[] bytes)
        {
            return ImageCompression.Compress(bytes, ImageFormat.Tiff);
        }

        public static byte[] CompressIco(this byte[] bytes)
        {
            return ImageCompression.Compress(bytes, ImageFormat.Icon);
        }

        public static byte[] CompressBmp(this byte[] bytes)
        {
            return ImageCompression.Compress(bytes, ImageFormat.Bmp);
        }
    }
}
