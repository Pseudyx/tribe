﻿using System;
using System.Diagnostics;

namespace Tribe.Core.Logging
{
    public static class ApplicationEventLog
    {
        static string LogName = "Application";
        static string SourceName = "Tribe Core";

        /// <summary>
        /// Write an Application Event Entry to the System Event Log
        /// </summary>
        /// <param name="message">Event text</param>
        /// <param name="type">Event LogEntry Type - Default: Information</param>
        /// <param name="content">raw data - Default: null</param>
        /// <param name="eventid">Event Id - Default: 0</param>
        /// <param name="subcateogry">Subcategory Id - Default: 0</param>
        public static void Write(string message, EventLogEntryType type = EventLogEntryType.Information, byte[] content = null, int eventid = 0, short subcateogry = 0)
        {
            if (!EventLog.SourceExists(SourceName))
            {
                EventLog.CreateEventSource(SourceName, LogName);
            }
            EventLog.WriteEntry(SourceName, message, type, eventid, subcateogry, content);
        }

        public static void Write(string format, params string[] args)
        {
            Write(string.Format(format, args));
        }

        public static void Write(Exception ex)
        {
            int errorNumber = ex.HResult;

            string errorMessage = ex.Message;
            string exceptionType = ex.GetType().Name;

            if (ex.GetBaseException() != null)
            {
                exceptionType += "->" + ex.GetBaseException().GetType().Name;
                errorMessage = ex.GetBaseException().Message;
            }

            Write("{0}", errorMessage, exceptionType);
        }
    }
}
