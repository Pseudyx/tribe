﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Tribe.Core.Service;

namespace Tribe.DAL.Services
{
    using Repository;

    public class AccountService : EntityService
    {
        public AccountService(EntityRepository entityDbContext) : base(entityDbContext) { }

        #region Profile

        public virtual async Task<Guid> AddProfile(Domain.Profile profile)
        {
            (_dbContext as EntityRepository).Profiles.Add(profile);
            await _dbContext.SaveChangesAsync();

            return profile.Id;
        }

        public virtual async Task UpdateProfile(Domain.Profile profile)
        {
            (_dbContext as EntityRepository).Profiles.Attach(profile);
            _dbContext.Entry(profile).State = EntityState.Modified;

            await _dbContext.SaveChangesAsync();
        }

        #endregion
    }
}
