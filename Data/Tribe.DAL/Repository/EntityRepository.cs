﻿using System;
using System.Data.Entity;

namespace Tribe.DAL.Repository
{
    using System.Data.Entity.ModelConfiguration.Conventions;
    using Core.Infrastructure;
    using Core.Repository;
    using Domain;

    public class EntityRepository : EntityDbContext
    {
        //Init
        public DbSet<Profile> Profiles { get; set; }

        //Organsiation
        public DbSet<Organisation> Organisations { get; set; }
        public DbSet<OrganisationRelationship> OrganisationRelationships { get; set; }
        public DbSet<OrganisationRelationshipType> OrganisationRelationshipTypes { get; set; }


        //Service Request
        public DbSet<ServiceRequest> ServiceRequests { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Task> Tasks { get; set; }


        //Mechanism
        public DbSet<Mechanism.Action> Actions { get; set; }
        public DbSet<Mechanism.Settings.ActionTrigger> Trigger { get; set; }
        public DbSet<Mechanism.Settings.ActionResult> Result { get; set; }

        protected override void OnModelCreating(System.Data.Entity.DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.HasDefaultSchema("Tribe");

            //Organisation
            modelBuilder.Configurations.Add(new OrganisationRelationshipEntityConfig());

            base.OnModelCreating(modelBuilder);
        }

    }
}
