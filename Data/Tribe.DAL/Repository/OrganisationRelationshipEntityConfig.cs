﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.DAL.Domain;

namespace Tribe.DAL.Repository
{
    public class OrganisationRelationshipEntityConfig : EntityTypeConfiguration<OrganisationRelationship>
    {
        public OrganisationRelationshipEntityConfig()
        {
            this.HasRequired<Organisation>(o => o.Parent)
                .WithMany()
                .HasForeignKey(k => k.ParentId)
                .WillCascadeOnDelete(false);

            this.HasRequired<Organisation>(o => o.Child)
                .WithMany()
                .HasForeignKey(k => k.ChildId)
                .WillCascadeOnDelete(false);

            this.HasRequired<OrganisationRelationshipType>(o => o.Relationship)
                .WithMany()
                .HasForeignKey(k => k.OrganisationRelationshipTypeId)
                .WillCascadeOnDelete(false);
                
        }
    }
}
