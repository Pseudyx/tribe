﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Tribe.Core.Repository;
using Tribe.Core.Service;

namespace Tribe.DAL
{
    public class ComponentInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

            container.Register(Classes.FromThisAssembly()
                .BasedOn<EntityDbContext>()
                .LifestyleTransient()
                .Configure(x => x.Named(x.Implementation.FullName)));

            container.Register(Classes.FromThisAssembly()
                .BasedOn<EntityService>()
                .LifestyleTransient()
                .Configure(x => x.Named(x.Implementation.FullName)));
        }
    }
}
