namespace Tribe.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProfileUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("Core.Profile", "FirstName", c => c.String());
            AddColumn("Core.Profile", "LastName", c => c.String());
            AddColumn("Core.Profile", "Email", c => c.String());
            AddColumn("Core.Profile", "Phone", c => c.String());
            AddColumn("Core.Profile", "ApplicationWebUserId", c => c.Guid());
            CreateIndex("Core.Profile", "ApplicationWebUserId");
            AddForeignKey("Core.Profile", "ApplicationWebUserId", "Core.User", "Id");
            DropColumn("Core.Profile", "Name");
        }
        
        public override void Down()
        {
            AddColumn("Core.Profile", "Name", c => c.String());
            DropForeignKey("Core.Profile", "ApplicationWebUserId", "Core.User");
            DropIndex("Core.Profile", new[] { "ApplicationWebUserId" });
            DropColumn("Core.Profile", "ApplicationWebUserId");
            DropColumn("Core.Profile", "Phone");
            DropColumn("Core.Profile", "Email");
            DropColumn("Core.Profile", "LastName");
            DropColumn("Core.Profile", "FirstName");
        }
    }
}
