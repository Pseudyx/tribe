namespace Tribe.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ServiceRequest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Tribe.Job",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        ServiceRequestId = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Tribe.ServiceRequest", t => t.ServiceRequestId, cascadeDelete: true)
                .Index(t => t.ServiceRequestId);
            
            CreateTable(
                "Tribe.ServiceRequest",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Number = c.String(),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Tribe.Task",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        JobId = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Tribe.Job", t => t.JobId, cascadeDelete: true)
                .Index(t => t.JobId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Tribe.Task", "JobId", "Tribe.Job");
            DropForeignKey("Tribe.Job", "ServiceRequestId", "Tribe.ServiceRequest");
            DropIndex("Tribe.Task", new[] { "JobId" });
            DropIndex("Tribe.Job", new[] { "ServiceRequestId" });
            DropTable("Tribe.Task");
            DropTable("Tribe.ServiceRequest");
            DropTable("Tribe.Job");
        }
    }
}
