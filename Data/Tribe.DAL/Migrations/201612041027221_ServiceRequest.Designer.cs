// <auto-generated />
namespace Tribe.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ServiceRequest : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ServiceRequest));
        
        string IMigrationMetadata.Id
        {
            get { return "201612041027221_ServiceRequest"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
