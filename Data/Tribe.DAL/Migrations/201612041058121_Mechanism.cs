namespace Tribe.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mechanism : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Mech.Action",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        ActionTriggerId = c.Guid(nullable: false),
                        ActionResultId = c.Guid(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Mech.ActionResult", t => t.ActionResultId, cascadeDelete: true)
                .ForeignKey("Mech.ActionTrigger", t => t.ActionTriggerId, cascadeDelete: true)
                .Index(t => t.ActionTriggerId)
                .Index(t => t.ActionResultId);
            
            CreateTable(
                "Mech.ActionResult",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Mech.ActionTrigger",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Mech.Action", "ActionTriggerId", "Mech.ActionTrigger");
            DropForeignKey("Mech.Action", "ActionResultId", "Mech.ActionResult");
            DropIndex("Mech.Action", new[] { "ActionResultId" });
            DropIndex("Mech.Action", new[] { "ActionTriggerId" });
            DropTable("Mech.ActionTrigger");
            DropTable("Mech.ActionResult");
            DropTable("Mech.Action");
        }
    }
}
