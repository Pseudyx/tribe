namespace Tribe.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Organisation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Tribe.OrganisationRelationship",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        OrganisationRelationshipTypeId = c.Guid(nullable: false),
                        ParentId = c.Guid(nullable: false),
                        ChildId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Tribe.Organisation", t => t.ChildId)
                .ForeignKey("Tribe.Organisation", t => t.ParentId)
                .ForeignKey("Tribe.OrganisationRelationshipType", t => t.OrganisationRelationshipTypeId)
                .Index(t => t.OrganisationRelationshipTypeId)
                .Index(t => t.ParentId)
                .Index(t => t.ChildId);
            
            CreateTable(
                "Tribe.Organisation",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Tribe.OrganisationRelationshipType",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Created = c.DateTime(nullable: false),
                        Modified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Tribe.OrganisationRelationship", "OrganisationRelationshipTypeId", "Tribe.OrganisationRelationshipType");
            DropForeignKey("Tribe.OrganisationRelationship", "ParentId", "Tribe.Organisation");
            DropForeignKey("Tribe.OrganisationRelationship", "ChildId", "Tribe.Organisation");
            DropIndex("Tribe.OrganisationRelationship", new[] { "ChildId" });
            DropIndex("Tribe.OrganisationRelationship", new[] { "ParentId" });
            DropIndex("Tribe.OrganisationRelationship", new[] { "OrganisationRelationshipTypeId" });
            DropTable("Tribe.OrganisationRelationshipType");
            DropTable("Tribe.Organisation");
            DropTable("Tribe.OrganisationRelationship");
        }
    }
}
