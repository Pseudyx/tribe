﻿using System;
using Tribe.Core.Entity;

namespace Tribe.DAL.Domain
{
    public class Organisation : Entity<Guid>
    {
        public string Name { get; set; }
    }
}
