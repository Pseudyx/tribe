﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Entity;

namespace Tribe.DAL.Domain
{
    public class Job : Entity<Guid>
    {
        public string Name { get; set; }

        public Guid ServiceRequestId { get; set; }
        public virtual ServiceRequest ServiceRequest { get; set; }

        public virtual ICollection<Task> Tasks { get; set; }

    }
}
