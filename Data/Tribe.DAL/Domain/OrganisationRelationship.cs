﻿using System;
using Tribe.Core.Entity;

namespace Tribe.DAL.Domain
{
    public class OrganisationRelationship : HIndex<Guid>
    {
        public Guid OrganisationRelationshipTypeId { get; set;}
        public virtual OrganisationRelationshipType Relationship { get; set; }

        //[ForeignKey("SenderId")]
        public Organisation Parent { get; set; }

        //[ForeignKey("SenderId")]
        public Organisation Child { get; set; }
    }


}
