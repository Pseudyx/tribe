﻿using System;
using System.Collections.Generic;
using Tribe.Core.Entity;

namespace Tribe.DAL.Domain
{
    public class OrganisationRelationshipType : Entity<Guid>
    {
        public string Name { get; set; }
    }
}
