﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Entity;

namespace Tribe.DAL.Domain
{
    public class Task : Entity<Guid>
    {
        public string Name { get; set; }

        public Guid JobId { get; set; }
        public virtual Job Job { get; set; }
    }
}
