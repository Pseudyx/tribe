﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Tribe.Core.Entity;
using Tribe.Core.Web.Infrastructure;

namespace Tribe.DAL.Domain
{
    [Table("Profile", Schema = "Core")]
    public class Profile : Entity<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Email { get; set; }
        public string Phone { get; set; }

        public Guid? ApplicationWebUserId { get; set; }
        public virtual ApplicationWebUser ApplicationUser { get; set; }

    }
}
