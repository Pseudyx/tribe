﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Entity;

namespace Tribe.DAL.Domain
{
    public class ServiceRequest : Entity<Guid>
    {
        public string Name { get; set; }
        public string Number { get; set; }

        public virtual ICollection<Job> Jobs { get; set; }
    }
}
