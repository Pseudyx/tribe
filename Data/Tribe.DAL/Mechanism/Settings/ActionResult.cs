﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Entity;

namespace Tribe.DAL.Mechanism.Settings
{
    [Table("ActionResult", Schema = "Mech")]
    public class ActionResult : ListEntity<Guid>
    {
        public string Name { get;set;}
    }
}
