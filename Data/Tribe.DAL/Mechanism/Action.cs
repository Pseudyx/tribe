﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tribe.Core.Entity;

namespace Tribe.DAL.Mechanism
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Settings;

    [Table("Action", Schema = "Mech")]
    public class Action : Entity<Guid>
    {
        public string Name { get; set; }

        public Guid ActionTriggerId { get; set; }
        public ActionTrigger Trigger { get; set; }

        public Guid ActionResultId { get; set; }
        public ActionResult Result { get; set; }
    }
}
